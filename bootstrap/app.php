<?php

session_start();

use Respect\Validation\Validator as v;

require __DIR__ . '/../vendor/autoload.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'determineRouteBeforeAppMiddleware' => true,
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'repairman',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'options' => [
                PDO::ATTR_CASE => PDO::CASE_LOWER
            ],
        ],
    ],
]);

$container = $app->getContainer();

/*************************************************************************************************************************************************
 *  
 *  Database
 *  Booting up the database connection, In this case we use Eloquent from Laravel
 *  https://laravel.com/docs/5.5/eloquent
 * 
 *************************************************************************************************************************************************/

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->bootEloquent();
$capsule->setAsGlobal();

/*************************************************************************************************************************************************
 *  
 *  Resources
 *  Adding resources to the container. These can be called later from any object that has a reference to $container
 * 
 *************************************************************************************************************************************************/

$container['db'] = function($container) use($capsule) 
{
    return $capsule;
};

$container['validator'] = function($container)
{
    return new App\Validations\Validator;
};
 /*************************************************************************************************************************************************
 *  
 *  Controllers
 *  The way page or resources are being called
 * @example
 * $container['Login'] = function($container)
 * {
 *    return new App\Controllers\LoginController($container);
 * };
 *************************************************************************************************************************************************/




 /*************************************************************************************************************************************************
 * 
 *  Middlewares.
 * 
 *  Example middleware call
 *  $app->add(new \App\Middlewares\ExMiddleware($container));
 * 
 *************************************************************************************************************************************************/


 /*************************************************************************************************************************************************
 *  
 *  Validation rules addons
 *  Adding custom validation rules to call with Respect later on
 * 
 *************************************************************************************************************************************************/

require __DIR__ . '/../app/Routes/routes.php';