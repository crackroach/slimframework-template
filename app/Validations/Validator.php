<?php

namespace App\Validations;

use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

/**
 * Validator to validate request's data
 */
class Validator
{

    protected $errors;

    /**
     * Run the validation
     */
    public function validate($request, array $rules) 
    {
        foreach ($rules as $field => $rule) {
            try 
            {
                $rule->setName(ucfirst($field))->assert($request->getParam($field));
            }
            catch(NestedValidationException $e) 
            {
                $this->errors[$field] = $e->getMessages();
            }
        }

        $_SESSION['validation_errors'] = $this->errors;

        return $this;
    }

    public function fails() 
    {
        return !empty($this->errors);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}