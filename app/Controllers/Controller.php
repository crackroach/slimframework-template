<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use App\Base;

/**
 * @author Alexis Diamond
 * @package App\Controller
 */
abstract class Controller extends Base
{
    // Enable this to be able to add styles in your page
    // use ViewStyles;
    // Enable this to be able to add scripts to your page
    // use ViewScripts;
    /**
     * A data container that is holding the data for an upcoming response
     */
    protected $data = ['data' => []];

    /**
     * Add some data to the data container
     * @param string $key the key of the value to add
     * @param any $value the value to add to the data bag
     */
    protected function addData(string $key, $values)
    {
        $this->data['data'][$key] = $values;
    }
}
