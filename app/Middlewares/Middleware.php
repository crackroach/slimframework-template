<?php

namespace App\Middlewares;
use App\Base;
/**
 * Base class for Middlewares
 */
abstract class Middleware extends Base
{
    /**
     * Invoke the middleware functionnaility
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param [type] $next response
     * @return response
     */
    abstract public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next);
}
