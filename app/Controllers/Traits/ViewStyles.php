<?php
trait ViewStyles {
    protected function addStyles(string $name)
    {
        $this->data['styles'][] = $name;
    }
}