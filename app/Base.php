<?php

namespace App;

/**
 * Base class for using slim containers with magic functions
 */
abstract class Base
{
    /**
     * A Slim container
     *
     * @var [type]
     */
    protected $container;

    /**
     * Constructor
     *
     * @param [type] $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Magic method for getting object from the container
     *
     * @param [type] $name the name of the resource to get from the container
     * @return any the requested resource
     */
    public function __get($name)
    {
        if ($this->container->{$name}) {
            return $this->container->{$name};
        }
    }
}
