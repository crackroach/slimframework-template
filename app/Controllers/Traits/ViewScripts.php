<?php
trait ViewScripts {
    protected function addScripts(string $name)
    {
        $this->data['scripts'][] = $name;
    }
}